
## Compile MSEide

UNITS=units

rm -f $UNITS/*.*
mkdir -p $UNITS

if [ "$1" = "debug" ] ; then
  OPTIONS="-Xg -gl -O-"
else
  OPTIONS="-O2 -XX -Xs -CX -B"
fi

echo "Compiling MSEide"

fpc \
-Fulib/common/* \
-Fulib/common/kernel \
-Filib/common/kernel \
-Fulib/common/kernel/linux \
-Fulib/addon/* \
-Filib/addon/* \
-FU$UNITS/ \
-l -Mobjfpc -Sh -Fcutf8 $OPTIONS apps/ide/mseide.pas \
&> build.log 

# MSEide+MSEgui Version 4.6.3 revised

## Download MSEgui from this repository

```
git clone --single-branch --branch maint https://gitlab.com/rchastain/mseide-msegui.git mseide463
```

## Compile MSEide and create desktop launcher and shell script

Change to MSEgui directory.

```
cd mseide463
```

Build MSEide.

```
sh build.sh
```

Create desktop launcher and shell script.

```
sh createlauncher.sh
```

Start MSEide!

```
sh mseide463-dev.sh
```

The first time you run MSEide, go to **Settings/Configure MSEide** and select MSEgui directory as a value for the *MSEDIR* variable.

![alt text](images/configure.png)

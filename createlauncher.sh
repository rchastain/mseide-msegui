
## Create desktop launcher and shell script for MSEide

## Revision number
REVISION="dev"
## Base name for desktop file and shell script
BASENAME="mseide463-$REVISION"
## Application full name
APP="MSEide 4.6.3 $REVISION"
## Script directory
SCRDIR="$(dirname "$(readlink -f "$0")")"
## Desktop directory
DDIR="$HOME/Desktop"

if [ ! -d $DDIR ] ;
then
  echo "Cannot find directory $DDIR"
  DDIR="$HOME/Bureau"
fi

## Create desktop launcher

if [ -d $DDIR ] ;
then
  FILE=$DDIR/$BASENAME.desktop
  echo "Creating desktop launcher $FILE"
  cat > $FILE << EOF
[Desktop Entry]
Version=1.0
Type=Application
Name=$APP
Comment=Pascal IDE
Exec=$SCRDIR/apps/ide/mseide --globstatfile=$SCRDIR/apps/ide/mseide.sta %F
Icon=$SCRDIR/msegui_64.png
Path=$SCRDIR/apps/ide
Terminal=false
StartupNotify=true
EOF
  echo "Making launcher executable"
  sudo chmod -R 777 $FILE
  echo "Done"
else
  echo "Cannot find directory $DDIR"
fi

## Create shell script

FILE=$BASENAME.sh
echo "Creating script $FILE"
cat > $FILE << EOF
MSEDIR=$SCRDIR
\$MSEDIR/apps/ide/mseide --globstatfile=\$MSEDIR/apps/ide/mseide.sta \$*
EOF
echo "Making script executable"
sudo chmod -R 777 $FILE
echo "Done"
